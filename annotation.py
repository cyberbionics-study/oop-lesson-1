class Annotation:
    def __init__(self, text: str):
        self.text = text

    def __str__(self):
        return f'{self.text}'

    def __repr__(self):
        return f'{self.text}'
