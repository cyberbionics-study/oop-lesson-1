from car_dealership import Car, Dealer


car1 = Car('Toyota', 'Supra A80 RZ-S', 2000, 90000)
car2 = Car('Ford', 'Shelby Mustang GT500KR', 2008, 125000)
car3 = Car('Chevrolet', 'Camaro SS', 2010, 75000)
car4 = Car('Audi', 'TT RS Coupe', 2022, 83000)
car5 = Car('Mercedes-Benz', 'SLS AMG Roadster (R197)', 2011, 110000)

car_list = [car1, car2, car3, car4, car5]

some_dealer = Dealer(car_list)

print(some_dealer)

requsted_car1 = car1
requsted_car2 = Car('Ford', 'Model T', 1912, 300)

some_dealer.sell(requsted_car1)
some_dealer.sell(requsted_car2)

print(some_dealer)