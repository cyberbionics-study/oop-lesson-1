class Car:
    def __init__(self, brand: str, model: str, manufacturing_year: int, price: int):
        self.brand = brand
        self.model = model
        self.manufacturing_year = manufacturing_year
        self.price = price

    def __str__(self):
        return f'\n{self.brand}, {self.model}, {self.manufacturing_year}, {self.price}\n'

    def __repr__(self):
        return f'\n{self.brand}, {self.model}, {self.manufacturing_year}, {self.price}\n'


class Dealer:
    def __init__(self, available_cars: list):
        self.available_cars = available_cars

    def __str__(self):
        return f'{self.available_cars}'

    def __repr__(self):
        return f'{self.available_cars}'

    def sell(self, request: Car):
        if request in self.available_cars:
            self.available_cars.remove(request)
            print(f"The deal is done. Here's your key")
        else:
            print(f"Dear customer, unfortunately we don't have the car you're searching.")
