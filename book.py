from annotation import Annotation


class Book:

    def __init__(self, author: str, title: str, pub_year: int, genre: str):
        self.author = author
        self.title = title
        self.pub_year = pub_year
        self.genre = genre
        self.annotations = []

    # def add_annotation(self, annotation: str):
    #     if not isinstance(annotation, Annotation):
    #         raise TypeError(
    #             f'add_annotation method expected Annotation type are/ but got {type(annotation)}'
    #         )
    #     self.annotations.append(annotation)

    def add_annotation(self, annotation: str):
        an = Annotation(annotation)
        self.annotations.append(an)


    def __str__(self):
        if self.annotations:
            result = ""
            for num, annotation in enumerate(self.annotations):
                result += f"{num}: {annotation}\n"
            return f'book: {self.title}, author: {self.author}, \npublishing year: {self.pub_year}, genre: {self.genre} \n{result}'
        return f'book: {self.title}, author: {self.author}, \npublishing year: {self.pub_year}, genre: {self.genre}'


    def __repr__(self):
        return f'book: {self.title}, \nauthor: {self.author}, \npublishing year: {self.pub_year}, \ngenre: {self.genre}'
